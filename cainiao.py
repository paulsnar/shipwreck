#!/usr/bin/env python3
# vim: set ts=2 sts=2 et sw=2 si:

import json
import time
import random
import redis
import requests
from bs4 import BeautifulSoup
from __version__ import VERSION

HEX_DIGITS = '0123456789abcdef'

r = redis.StrictRedis(encoding='utf-8', decode_responses=True)
RP = 'lv.paulsnar.daemons.shipwreck'

trackable_consignments = r.smembers(RP + '.cainiao._all')

for consignment in trackable_consignments:
  resp = requests.get(
    'https://global.cainiao.com/detail.htm?' \
      'mailNoList={0}'.format(consignment),
    headers={
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) ' \
        'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 ' \
        'Safari/537.36'
    },
    proxies={
      'http': 'socks5://127.0.0.1:9050',
      'https': 'socks5://127.0.0.1:9050',
    },
    cookies={
      'lang': 'en',
    },
  )
  soup = BeautifulSoup(resp.text, 'html.parser')

  data = soup.find('textarea', id='waybill_list_val_box')
  data = json.loads(data.string.strip())
  data = data['data'][0]

  if data.get('errorCode') == 'RESULT_EMPTY':
    last_status = None
  else:
    last_status = data['latestTrackingInfo']['desc']
  stored_status = r.get(RP + '.cainiao.{0}.last_status'.format(consignment))

  if last_status is not None and last_status != stored_status:
    msg = {
      'id': consignment + '_',
      'consignment': consignment,
      'status': last_status,
      'tracking_link': 'https://global.cainiao.com/detail.htm?' \
        'mailNoList={0}'.format(consignment)
    }

    subscribers = r.smembers(RP + '.cainiao.{0}.subscribers' \
      .format(consignment))
    for subscriber in subscribers:
      msg_personal = msg.copy()
      msg_personal['id'] += \
        ''.join([random.choice(HEX_DIGITS) for _ in range(0, 8)])
      msg_personal['created_at'] = int(time.time())
      msg_personal['target'] = int(subscriber)

      r.lpush(RP + '.tg.pending', json.dumps(msg_personal))

    r.set(RP + '.cainiao.{0}.last_status'.format(consignment), last_status)

  time.sleep(1)
