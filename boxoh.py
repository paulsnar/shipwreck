#!/usr/bin/env python3
# vim: set ts=2 sts=2 et sw=2 si:

import json
import time
import random
import redis
import requests
from bs4 import BeautifulSoup
from __version__ import VERSION

HEX_DIGITS = '0123456789abcdef'

r = redis.StrictRedis(encoding='utf-8', decode_responses=True)
RP = 'lv.paulsnar.daemons.shipwreck'

trackable_consignments = r.smembers(RP + '.boxoh._all')

for consignment in trackable_consignments:
  resp = requests.get(
    'http://www.boxoh.com/?rss=1&t={0}'.format(consignment),
    headers={
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) ' \
        'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 ' \
        'Safari/537.36 shipwreck/{version} <paulsnar@paulsnar.lv>'.format(
          version=VERSION)
    }
  )
  soup = BeautifulSoup(resp.text, 'html.parser')

  items = soup.rss.channel.find_all('item')
  last_item = items[0]
  if 'error' in last_item.title.string.lower():
    last_status = None
  else:
    last_status = ':'.join(last_item.title.string.split(':')[2:]).lstrip()

  stored_status = r.get(RP + '.boxoh.{0}.last_status'.format(consignment))

  if last_status is not None and last_status != stored_status:
    msg = {
      'id': consignment + '_',
      'consignment': consignment,
      'status': last_status,
      'tracking_link': 'http://www.boxoh.com/?t={0}'.format(consignment),
    }

    subscribers = r.smembers(RP + '.boxoh.{0}.subscribers'.format(consignment))
    for subscriber in subscribers:
      msg_personal = msg.copy()
      msg_personal['id'] += \
        ''.join([random.choice(HEX_DIGITS) for _ in range(0, 8)])
      msg_personal['created_at'] = int(time.time())
      msg_personal['target'] = int(subscriber)

      r.lpush(RP + '.tg.pending', json.dumps(msg_personal))

    r.set(RP + '.boxoh.{0}.last_status'.format(consignment), last_status)

  time.sleep(1)
