#!/usr/bin/env python3
# vim: set ts=2 sts=2 et sw=2 si:

import json
import time
import random
import redis
import requests
from __version__ import VERSION

HEX_DIGITS = '0123456789abcdef'

r = redis.StrictRedis(encoding='utf-8', decode_responses=True)
RP = 'lv.paulsnar.daemons.shipwreck'

trackable_consignments = r.smembers(RP + '.dhl._all')

for consignment in trackable_consignments:
  resp = requests.get(
    'http://www.dhl.com/shipmentTracking?AWB={0}'.format(consignment),
    headers={
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) ' \
        'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 ' \
        'Safari/537.36 shipwreck/{version}'.format(version=VERSION)
    }
  )
  data = resp.json()

  if 'errors' in data:
    print(
      'Failed tracking {0}: errors: {1}'.format(consignment, data['errors']))
    time.sleep(1)
    continue

  data = data['results'][0]
  last_checkpoint = 0
  _l = None
  for checkpoint in data['checkpoints']:
    if checkpoint['counter'] > last_checkpoint:
      last_checkpoint = checkpoint['counter']
      _l = checkpoint
  last_checkpoint = _l
  del _l

  stored_checkpoint = r.get(
    RP + '.dhl.{0}.last_checkpoint'.format(consignment))
  if stored_checkpoint is not None:
    stored_checkpoint = int(stored_checkpoint)

  if last_checkpoint is None:
    time.sleep(1)
    continue

  if last_checkpoint['counter'] != stored_checkpoint:
    msg = {
      'id': consignment + '_',
      'consignment': consignment,
      'status': last_checkpoint['description'],
      'tracking_link': \
        'http://www.dhl.com/en/express/tracking.html?AWB={0}'.format(
          consignment),
    }

    subscribers = r.smembers(RP + '.dhl.{0}.subscribers'.format(consignment))
    for subscriber in subscribers:
      msg_personal = msg.copy()
      msg_personal['id'] += \
        ''.join([random.choice(HEX_DIGITS) for _ in range(0, 8)])
      msg_personal['created_at'] = int(time.time())
      msg_personal['target'] = int(subscriber)

      r.lpush(RP + '.tg.pending', json.dumps(msg_personal))

    r.set(RP + '.dhl.{0}.last_checkpoint'.format(consignment),
      last_checkpoint['counter'])

  time.sleep(1)
