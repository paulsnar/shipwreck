#!/usr/bin/env python3
# vim: set ts=2 sts=2 et sw=2 si:

import json
import time
import random
import redis
import requests
from bs4 import BeautifulSoup
from __version__ import VERSION

HEX_DIGITS = '0123456789abcdef'

r = redis.StrictRedis(encoding='utf-8', decode_responses=True)
RP = 'lv.paulsnar.daemons.shipwreck'

trackable_consignments = r.smembers(RP + '.yunpost._all')

for consignment in trackable_consignments:
  resp = requests.post(
    'http://www.yuntrack.com/Track/PartialDetail',
    data={ 'numbers[]': consignment },
    headers={
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) ' \
        'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 ' \
        'Safari/537.36',
      'referer': 'http://www.yuntrack.com/Track/Detail',
      'x-requested-with': 'XMLHttpRequest',
    },
    proxies={
      'http': 'socks5://127.0.0.1:9050',
      'https': 'socks5://127.0.0.1:9050',
    }
  )
  soup = BeautifulSoup(resp.text, 'html.parser')

  tbl = soup.table
  if tbl.find('span', class_='track_detail_NotFound') is not None:
    last_status = None
  else:
    cells = tbl.find_all('td')
    last_status = ','.join(cells[5].get_text().strip().split(',')[1:])

  stored_status = r.get(RP + '.yunpost.{0}.last_status'.format(consignment))

  if last_status != stored_status:
    msg = {
      'id': consignment + '_',
      'consignment': consignment,
      'status': last_status,
      'tracking_link': 'http://www.yuntrack.com/Track/Detail/{0}'.format(
          consignment)
    }

    subscribers = r.smembers(RP + '.yunpost.{0}.subscribers'.format(consignment))
    for subscriber in subscribers:
      msg_personal = msg.copy()
      msg_personal['id'] += \
        ''.join([random.choice(HEX_DIGITS) for _ in range(0, 8)])
      msg_personal['created_at'] = int(time.time())
      msg_personal['target'] = int(subscriber)

      r.lpush(RP + '.tg.pending', json.dumps(msg_personal))

    r.set(RP + '.yunpost.{0}.last_status'.format(consignment), last_status)

  time.sleep(1)
