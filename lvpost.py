#!/usr/bin/env python3
# vim: set ts=2 sts=2 et sw=2 si:

import json
import time
import random
import redis
import requests
from bs4 import BeautifulSoup
from __version__ import VERSION

HEX_DIGITS = '0123456789abcdef'

r = redis.StrictRedis(encoding='utf-8', decode_responses=True)
RP = 'lv.paulsnar.daemons.shipwreck'

trackable_consignments = r.smembers(RP + '.lvpost._all')

for consignment in trackable_consignments:
  resp = requests.get(
    'https://track.pasts.lv/consignment/tracking?' \
      'type=pastsLv&lang=lv&id={0}'.format(consignment),
    headers={
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) ' \
        'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 ' \
        'Safari/537.36 shipwreck/{version}'.format(version=VERSION)
    }
  )
  soup = BeautifulSoup(resp.text, 'html.parser')

  statuses = list(map(lambda el: el.h4.string,
    soup.find_all('td', class_='status')))
  if len(statuses) == 0:
    last_status = None
  else:
    last_status = statuses[0]

  stored_status = r.get(RP + '.lvpost.{0}.last_status'.format(consignment))

  if last_status is not None and last_status != stored_status:
    msg = {
      'id': consignment + '_',
      'consignment': consignment,
      'status': last_status,
      'tracking_link': 'https://track.pasts.lv/consignment/tracking?' \
        'type=pastsLv&lang=lv&id={0}'.format(consignment)
    }

    subscribers = r.smembers(RP + '.lvpost.{0}.subscribers'.format(consignment))
    for subscriber in subscribers:
      msg_personal = msg.copy()
      msg_personal['id'] += \
        ''.join([random.choice(HEX_DIGITS) for _ in range(0, 8)])
      msg_personal['created_at'] = int(time.time())
      msg_personal['target'] = int(subscriber)

      r.lpush(RP + '.tg.pending', json.dumps(msg_personal))

    r.set(RP + '.lvpost.{0}.last_status'.format(consignment), last_status)

  time.sleep(1)
