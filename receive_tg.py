#!/usr/bin/env python3
# vim: set ts=2 sts=2 et sw=2 si:

import os
import os.path as path
import json
import time
import redis
import requests
from dotenv import load_dotenv
from time import sleep
import msgpack

load_dotenv( path.join( path.dirname(__file__), '.env' ) )

TELEGRAM_API_KEY = os.environ.get('TELEGRAM_API_KEY', None)
if TELEGRAM_API_KEY is None:
  raise Exception('Invalid Telegram API key specified.')

r = redis.StrictRedis(encoding='utf-8', decode_responses=True)
RP = 'lv.paulsnar.daemons.shipwreck'

last_id = -1
timeouts_left = { 'connect': 3, 'read': 5 }
while True:
  params = {
    'timeout': 60
  }

  if last_id > -1:
    params['offset'] = last_id + 1

  try:
    resp = requests.get(
      'https://api.telegram.org/bot{}/getUpdates'.format(TELEGRAM_API_KEY),
      params=params,
      timeout=(5, params['timeout'] + 5)
    )
  except requests.ConnectTimeout:
    print('[timeout] Connect timeout...')
    timeouts_left['connect'] -= 1
    if timeouts_left['connect'] == 0:
      print('Aborting.')
      exit(1)
  except requests.ReadTimeout:
    print('[timeout] Read timeout...')
    timeouts_left['read'] -= 1
    if timeouts_left['read'] == 0:
      print('Aborting.')
      exit(1)

  timeouts_left['connect'] = 3
  timeouts_left['read'] = 5
  rj = resp.json()

  if rj['ok'] != True:
    print('Failed: {}'.format(rj['description']))
    exit(1)

  for update in rj['result']:
    r.lpush(RP + '.tg.incoming', msgpack.packb(update))
    if update['update_id'] > last_id:
      last_id = update['update_id']
