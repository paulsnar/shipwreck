#!/usr/bin/env python3
# vim: set ts=2 sts=2 et sw=2 si:

import os
import os.path as path
import json
import time
import redis
import requests
from html import escape
from dotenv import load_dotenv
from time import sleep
import msgpack

load_dotenv( path.join( path.dirname(__file__), '.env' ) )

TELEGRAM_API_KEY = os.environ.get('TELEGRAM_API_KEY', None)
if TELEGRAM_API_KEY is None:
  raise Exception('Invalid Telegram API key specified.')

r = redis.StrictRedis(encoding='utf-8', decode_responses=True)
RP = 'lv.paulsnar.daemons.shipwreck'

while True:
  msg_raw = r.brpoplpush(RP + '.tg.pending', RP + '.tg.pending', 0)
  msg = json.loads(msg_raw)
  msg['processing_since'] = int(time.time())

  if msg['status'] is None:
    r.lrem(RP + '.tg.pending', 0, msg_raw)
    time.sleep(1)
    continue

  with r.pipeline() as p:
    p.lrem(RP + '.tg.pending', 0, msg_raw)
    msg_raw = json.dumps(msg)
    p.lpush(RP + '.tg.processing', msg_raw)
    p.execute()

  payload = {
    'chat_id': msg['target'],
    'parse_mode': 'HTML',
    'text': 'Paciņai <code>{consignment}</code> statuss mainīts uz:\n' \
      '<b>{status}</b>'.format(
        consignment=escape(msg['consignment']),
        status=escape(msg['status'])
      ),
  }

  if msg['tracking_link'] is not None:
    payload['reply_markup'] = {
      'inline_keyboard': [[{
        'text': 'Skatīt vairāk',
        'url': msg['tracking_link'],
      }]]
    }

  resp = requests.post(
    'https://api.telegram.org/bot{0}/sendMessage'.format(TELEGRAM_API_KEY),
    json=payload)
  if resp.status_code != 200:
    rj = resp.json()
    msg['attempts'] = msg.get('attempts', 0) + 1
    msg['last_attempted_at'] = int(time.time())
    msg['last_failure_msg'] = rj['description']
    del msg['processing_since']

    if msg['attempts'] > 3:
      r.lrem(RP + '.tg.processing', 0, msg_raw)
      r.lpush(RP + '.tg.failed', msgpack.packb(msg))
    else:
      r.lrem(RP + '.tg.processing', 0, msg_raw)
      r.lpush(RP + '.tg.pending', json.dumps(msg))
  else:
    resp_c = resp.json()['result']
    msg['message'] = {
      'id': resp_c['message_id'],
      'chat': resp_c['chat']['id'],
      'date': resp_c['date'],
    }
    del msg['processing_since']
    del msg['target'] # specified above
    r.lrem(RP + '.tg.processing', 0, msg_raw)
    r.lpush(RP + '.tg.sent', msgpack.packb(msg))

  time.sleep(1)
